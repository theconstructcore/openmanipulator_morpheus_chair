#!/usr/bin/env python


import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi
from sensor_msgs.msg import JointState
from std_msgs.msg import Header, Bool


class MoveGroupSafeWatcher(object):

    def __init__(self):
        super(MoveGroupSafeWatcher, self).__init__()

        
        

        ## BEGIN_SUB_TUTORIAL setup
        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)

        ## Instantiate a `RobotCommander`_ object. This object is the outer-level interface to
        ## the robot:
        robot = moveit_commander.RobotCommander()

        ## Instantiate a `PlanningSceneInterface`_ object.  This object is an interface
        ## to the world surrounding the robot:
        scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to one group of joints.  In this case the group is the joints in the Panda
        ## arm so we set ``group_name = panda_arm``. If you are using a different robot,
        ## you should change this value to the name of your robot arm planning group.
        ## This interface can be used to plan and execute motions on the Panda:
        group_name = "arm"
        print("Waiting MAX 5 minutes >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        group = moveit_commander.MoveGroupCommander(group_name, wait_for_servers=300.0)
        print("Waiting MAX 5 minutes DONE >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
        ## Getting Basic Information
        ## ^^^^^^^^^^^^^^^^^^^^^^^^^
        # We can get the name of the reference frame for this robot:
        planning_frame = group.get_planning_frame()
        rospy.loginfo("============ Reference frame: %s" % planning_frame)

        # We can also rospy.loginfo the name of the end-effector link for this group:
        eef_link = group.get_end_effector_link()
        rospy.loginfo("============ End effector: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        rospy.loginfo("============ Robot Groups:"+str(robot.get_group_names()))

        # Sometimes for debugging it is useful to rospy.loginfo the entire state of the
        # robot:
        rospy.loginfo("============ rospy.loginfoing robot state")
        rospy.loginfo(robot.get_current_state())
        rospy.loginfo ("")

        # Misc variables

        self.group = group

        rospy.Subscriber('/goal_dynamixel_position',JointState, self.goal_dynamixel_callback)

        self.joint_states_topic_name = '/joint_states'
        self._check_join_states_ready()
        sub = rospy.Subscriber(self.joint_states_topic_name, JointState, self.joint_states_callback)

        self.goal_dynamixel_position_publisher = rospy.Publisher('/goal_dynamixel_position',
                                                                    JointState,
                                                                    queue_size=1)
        
        self.safe_positions_results_publisher = rospy.Publisher('/safe_positions_results',
                                                                    Bool,
                                                                    queue_size=1)

    def _check_join_states_ready(self):
        self.joint_states_msg = None
        rospy.loginfo("Waiting for "+self.joint_states_topic_name+" to be READY...")
        while self.joint_states_msg is None and not rospy.is_shutdown():
            try:
                self.joint_states_msg = rospy.wait_for_message(self.joint_states_topic_name, JointState, timeout=5.0)
                rospy.logdebug("Current "+self.joint_states_topic_name+" READY=>")

            except:
                rospy.logerr("Current "+self.joint_states_topic_name+" not ready yet, retrying ")

    def create_safe_joint_values(self):

        self.safe_joints_0 = JointState()

        h = Header()
        h.stamp = rospy.Time.now()  # Note you need to call rospy.init_node() before this will work
        h.frame_id = self.joint_states_msg.header.frame_id

        self.safe_joints_0.header = h
        self.safe_joints_0.name = self.joint_states_msg.name
        self.safe_joints_0.position = joint_position_home = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

    def create_safe_joint_values_current(self):
        """
        We create safe based on the current joints
        """
        self.safe_joints_0 = JointState()

        h = Header()
        h.stamp = rospy.Time.now()  # Note you need to call rospy.init_node() before this will work
        h.frame_id = self.joint_states_msg.header.frame_id

        self.safe_joints_0.header = h
        self.safe_joints_0.name = self.joint_states_msg.name
        self.safe_joints_0.position = self.joint_states_msg.position


    def joint_states_callback(self,msg):
        """
        rosmsg show sensor_msgs/JointState
            std_msgs/Header header
              uint32 seq
              time stamp
              string frame_id
            string[] name
            float64[] position
            float64[] velocity
            float64[] effort

        :param msg:
        :return:
        """
        self.joint_states_msg = msg

    def goal_dynamixel_callback(self,msg):
        
        names = msg.name
        pos = msg.position

        # rospy.loginfo(str(names))
        rospy.loginfo(str(pos))

        plan_ok = self.joint_traj_set_plan_generic(pos)
        plan_ok_msg = Bool()
        plan_ok_msg.data = plan_ok

        if not plan_ok:
            # The Joint Sent is not safe, we have to stop it
            # rospy.logerr("SAFE JOINTS PUBLISH STARTED")
            self.create_safe_joint_values()
            # self.create_safe_joint_values_current()
            self.goal_dynamixel_position_publisher.publish(self.safe_joints_0)
        else:
            # rospy.logwarn("SAFE JOINTS.......")
            pass

        self.safe_positions_results_publisher.publish(plan_ok_msg)


    def joint_traj_set_plan_generic(self, joint_positions):
        """
        We only chek the 6 first ones, the others are related to the gripper that we dont check
        """

        joint_goal = self.group.get_current_joint_values()
        i = 0
        for joint_value in joint_positions:
            joint_goal[i] = joint_value
            i += 1
            if i >= 5:
                break
        
        # rospy.loginfo(joint_goal)
        
        self.group.set_joint_value_target(joint_goal)

        plan_ok = self.plan_trajectory()

        # rospy.logwarn("Plan Status==="+str(plan_ok))

        return plan_ok


    def joint_traj_set_plan(self):

        joint_goal = self.group.get_current_joint_values()
        rospy.loginfo ("Group Vars:")
        
        joint_goal[0] = 0
        joint_goal[1] = -pi/4
        joint_goal[2] = 0
        joint_goal[3] = -pi/2
        joint_goal[4] = 0
        joint_goal[5] = pi/3
        rospy.loginfo (joint_goal)

        self.group.set_joint_value_target(joint_goal)

    def joint_traj_set_error_plan(self):

        joint_goal = self.group.get_current_joint_values()
        rospy.loginfo ("Group Vars:")
        rospy.loginfo("Length="+str(len(joint_goal)))
        joint_goal[0] = -1.76
        joint_goal[1] = 2.00
        joint_goal[2] = 0
        joint_goal[3] = 0
        joint_goal[4] = 0
        joint_goal[5] = 0
        rospy.loginfo (joint_goal)

        self.group.set_joint_value_target(joint_goal)

    def plan_trajectory(self):

        self.plan = self.group.plan()
        plan_ok = self.plan[0]
        
        return plan_ok

        
    def execute_trajectory(self):
        self.group.go(wait=True)
        group.stop()

    def plan_only_traj(self):
        rospy.loginfo("Planning Valid Joint Config")
        self.joint_traj_set_plan()
        plan_result = self.plan_trajectory()
        rospy.loginfo("Plan Result="+str(plan_result))

        rospy.loginfo("Planning ERROR Joint Config")
        self.joint_traj_set_error_plan()
        plan_result = self.plan_trajectory()
        rospy.loginfo("Plan Result="+str(plan_result))



def main():
    rospy.init_node('move_group_watcher',
                    anonymous=True)
    try:
        move_group_watcher_obj = MoveGroupSafeWatcher()

        rospy.loginfo("============ Press `Enter` to execute a movement using a joint state goal ...")
        move_group_watcher_obj.plan_only_traj()

        rospy.spin()

        rospy.loginfo("============ Python move_group_watcher_obj demo complete!")
    except rospy.ROSInterruptException:
        return
    except KeyboardInterrupt:
        return

if __name__ == '__main__':
    main()